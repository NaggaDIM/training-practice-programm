#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication
from GUI import MainAppWindow


if __name__ == '__main__':
	app = QApplication(sys.argv)
	app.setStyle("Fusion")
	ui = MainAppWindow()
	sys.exit(app.exec_())