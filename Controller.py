#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os.path
import re
import yaml
from Models import BaseModel, Configuration, Role, User, Product, ProductLog, Order, OrderLog

class Controller(object):
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.user = None
		if not os.path.isfile('application.db'):
			BaseModel().init_database()

	def get_saved_user(self):
		try: 
			with open("config.yml", "r") as config_file: return yaml.safe_load(config_file)
		except Exception as e: return {'user': { 'login': None, 'password': None }}

	def set_saved_user(self, user):
		try: 
			with open("config.yml", 'w') as f: f.write(yaml.safe_dump({'user': {'login': user.login, 'password': user.password}}))
		except Exception as e: print('USER SAVE CONFIG ERROR')

	def del_saved_user(self):
		try: 
			with open("config.yml", 'w') as f: f.write(yaml.safe_dump({'user': {'login': None, 'password': None}}))
		except Exception as e: print('USER DELETE CONFIG ERROR')

	def validate_auto_login(self):
		config = self.get_saved_user()
		if config and 'user' in config:
			user = config['user']
			if 'login' in user and 'password' in user:
				login = user['login']
				password = user['password']
				if login and password:
					if self.login_with_hash(login, password):
						return True
					else: return False
				else: return False
			else: return False
		else: return False

	def login(self, login, password, remember_me:bool=False):
		if not login or not password: return False

		try:
			self.user = User.select().where((User.login == login) & (User.password == User.hash_password(password))).get()
			if remember_me: self.set_saved_user(self.user)
			return True	
		except Exception as e:
			return False

	def login_with_hash(self, login, hashed_password):
		if not login or not hashed_password: return False

		try:
			self.user = User.select().where((User.login == login) & (User.password == hashed_password)).get()
			self.set_saved_user(self.user)
			return True
		except Exception as e:
			return False

	def register(self, name, login, password, conifirm_password):
		errors = {}
		if not name: errors['name'] = 'Введите Имя'
		if name and len(name) < 4: errors['name'] = 'Требуеться минимум 3 символа'
		if not login: errors['login'] = 'Введите Логин'
		if login and User.select().where(User.login == login).count() > 0: errors['login'] = 'Этот логин уже занят'
		if not password: errors['password'] = 'Введите пароль'
		if not conifirm_password: errors['conifirm_password'] = 'Подтвердите пароль'
		if password and conifirm_password and password != conifirm_password: errors['conifirm_password'] = 'Пароли не совпадают'
		if password and conifirm_password and password == conifirm_password and not re.findall(User.PASSWORD_REGEX_PATTERN, password): errors['password'] = User.PASSWORD_REGEX_INVALID_MESSAGE

		if not errors:
			self.user = User.create(name=name, login=login, role=Role.CLIENT, password=User.hash_password(password))
			return True
		else: return errors
		
	def logout(self):
		self.user = None
		self.del_saved_user()

	def change_user_name(self, new_name):
		if not new_name: return 'Введите имя'
		if len(new_name) < 4: return 'Имя должно состоять минимум из 4 символов'
		if new_name == self.user.name: return 'Новое имя совпадает с текущим'

		try:
			self.user.name = new_name
			self.user.save()
			return True
		except Exception as e: return 'Что-то пошло не так...'

	def change_user_phone(self, new_phone):
		if not new_phone: return 'Введите телефон'
		if len(new_phone) < 10: return 'Телефон должен состоять минимум из 10 символов'
		if new_phone == self.user.phone: return 'Новый телефон совпадает с текущим'

		try:
			self.user.phone = new_phone
			self.user.save()
			return True
		except Exception as e: return 'Что-то пошло не так...'

	def change_user_address(self, new_address):
		if not new_address: return 'Введите адресс'
		if len(new_address) < 4: return 'Адресс должен состоять минимум из 4 символов'
		if new_address == self.user.address: return 'Новый адресс совпадает с текущим'

		try:
			self.user.address = new_address
			self.user.save()
			return True
		except Exception as e: return 'Что-то пошло не так...'

	def change_user_password(self, old_password, new_password, conifirm_password):
		if not old_password: return 'Поле "Старый пароль" обязательно для заполнения'
		if not new_password: return 'Поле "Новый пароль" обязательно для заполнения'
		if not conifirm_password: return 'Поле "Подтверждение" обязательно для заполнения'

		if User.hash_password(old_password) != self.user.password: return 'Старый пароль введён неверно'

		if new_password != conifirm_password: return 'Поля "Новый пароль" и "Подтверждение" должны совпадать'

		if not re.findall(User.PASSWORD_REGEX_PATTERN, new_password): return User.PASSWORD_REGEX_INVALID_MESSAGE

		try:
			self.user.password = User.hash_password(new_password)
			self.user.save()
			return True
		except Exception as e: return 'Что-то пошло не так...'
		
	def admin_get_users_list(self):
		if self.user.is_admin(): return User.select()
		else: return None

	def get_roles_list(self):
		return Role.select()

	def admin_change_user_role(self, user, role):
		if self.user.is_admin():
			user.role = role
			user.save()
			return True
		else: return False

	def get_theme(self): return Configuration.get_current_theme()
	def set_theme(self, theme):
		if Configuration.THEME_DARK <= theme <= Configuration.THEME_LIGHT:
			Configuration.set_current_theme(theme)
			return True
		return False
	def get_themes(self): return Configuration.THEMES

	def get_products(self): return self.get_storekeeper_products() if self.user.is_storekeeper() else self.get_client_products()
	def get_client_products(self): return Product.select().where(Product.count > 0)
	def get_storekeeper_products(self): return Product.select()

	def user_make_order(self, product_id, count):
		if self.user.is_client():
			try:
				product = Product.get(product_id)
			except Exception as e:
				return 'Упс... Что-то пошло не так...'

			if count < 1: return 'Недопустимое значение поля "Количество"\nКоличество не должно быть меньше 1'
			if count > product.count: return 'Недопустимое значение поля "Количество"\nКоличество не должно превышать {}'.format(product.count)

			try:
				product.count -= count
				product.save()

				ProductLog.create(product=product, user=self.user, action=ProductLog.ACTION_ORDERED)

				order = Order.create(user=self.user, product=product, count=count, status=Order.STATUS_CREATED)
				OrderLog.create(order=order, user=self.user, action=OrderLog.ACTION_CREATED)
				return True
			except Exception as e:
				return 'Упс... Что-то пошло не так...'
		else:
			return 'Функция недоступна для вас!'

	def storekeeper_create_product(self, product_name:str, product_count:int):
		if self.user.is_storekeeper():
			if not product_name: return 'Поле "Наименование" не заполнено'
			if not product_count: return 'Поле "Количество" не заполнено'
			if product_count < 0: return 'Поле "Количество" не может иметь отрицательное значение'

			try:
				product = Product.create(name=product_name, count=product_count)
				ProductLog.create(product=product, user=self.user, action=ProductLog.ACTION_CREATED)
				return True
			except Exception as e:
				return 'Упс... Что-то пошло не так...'
		else:
			return 'Функция недоступна для вас!'

	def storekeeper_update_product(self, product_id:int, product_name:str, product_count:int):
		if self.user.is_storekeeper():
			try:
				product = Product.get(product_id)
			except Exception as e:
				return 'Упс... Что-то пошло не так...'

			if not product_name: return 'Поле "Наименование" не заполнено'
			if not product_count: return 'Поле "Количество" не заполнено'
			if product_count < 0: return 'Поле "Количество" не может иметь отрицательное значение'

			try:
				product.name = product_name
				product.count = product_count
				product.save()

				ProductLog.create(product=product, user=self.user, action=ProductLog.ACTION_UPDATATED)
				return True
			except Exception as e:
				return 'Упс... Что-то пошло не так...'
		else:
			return 'Функция недоступна для вас!'

	def get_orders(self): return self.get_manager_orders() if self.user.is_manager() else self.get_client_orders()
	def get_client_orders(self): return Order.select().where(Order.user == self.user)
	def get_manager_orders(self): return Order.select()
	def get_order_status(self, status:int=None):
		if not isinstance(status, int): return Order.STATUSES
		else: return Order.STATUSES[status]

	def client_cancel_order(self, order_id:int):
		if self.user.is_client():
			try:
				order = Order.get(order_id)
				if order.user.id != self.user.id: return 'Вы не можете отменить чужой заказ'
				order.status = Order.STATUS_CANCELED
				order.save()
				product = order.product
				product.count += order.count
				product.save()
				OrderLog.create(order=order, user=self.user, action=OrderLog.ACTION_CANCELED)
				ProductLog.create(product=product, user=self.user, action=ProductLog.ACTION_UPDATATED)
			except Exception as e:
				return 'Упс... Что-то пошло не так...'
		else:
			return 'Функция недоступна для вас!'

	def manager_change_order_status(self, order_id, status):
		if self.user.is_manager():
			try:
				order = Order.get(order_id)
				if order.status == status: return "Вы пытаетесь поставить заказу его текщий статус"
				if status < Order.STATUS_CREATED or status > Order.STATUS_CANCELED: return 'Недопустимый статус'
				if order.status == Order.STATUS_CANCELED:
					order.product.count += order.count
				if status == Order.STATUS_CANCELED:
					order.product.count -= order.count
				order.status = status
				order.save()
				if order.status == Order.STATUS_CANCELED or status == Order.STATUS_CANCELED: ProductLog.create(product=order.product, user=self.user, action=ProductLog.ACTION_UPDATATED)
				OrderLog.create(order=order, user=self.user, action=status)
				return True
			except Exception as e:
				return 'Упс... Что-то пошло не так...'
		else: return 'Функция недоступна для вас!'
