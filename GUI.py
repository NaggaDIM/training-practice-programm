#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QMainWindow, QWidget, QTabWidget, QVBoxLayout, QHBoxLayout, QScrollArea, QMessageBox, QGroupBox, QLabel, QLineEdit, QPushButton, QCheckBox, QComboBox, QSlider
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtCore import Qt
from Controller import Controller
from Models import Role, User, Configuration


class MainAppWindow(QMainWindow):
	def __init__(self, parent=None):
		super().__init__()
		self.controller = Controller(self)
		self.palette = QPalette()
		self.window_widget = None
		self.toggle_theme(self.controller.get_theme())
		if self.controller.validate_auto_login(): self.get_user_widget()
		else: 
			self.controller.del_saved_user()
			self.update_widget(LoginWidget(self))

	def get_user_widget(self):
		self.update_widget(MainWidget(self))
		self.resize(0, 0)

	def get_login_widget(self):
		self.update_widget(LoginWidget(self))
		self.resize(0, 0)
	
	def update_widget(self, widget:QWidget):
		self.window_widget = widget
		self.setCentralWidget(self.window_widget)
		self.show()

	def toggle_theme(self, theme):
		if theme == Configuration.THEME_DARK:
			self.palette.setColor(QPalette.Window, QColor(53, 53, 53))
			self.palette.setColor(QPalette.WindowText, Qt.white)
			self.palette.setColor(QPalette.Base, QColor(25, 25, 25))
			self.palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
			self.palette.setColor(QPalette.ToolTipBase, Qt.white)
			self.palette.setColor(QPalette.ToolTipText, Qt.white)
			self.palette.setColor(QPalette.Text, Qt.white)
			self.palette.setColor(QPalette.Button, QColor(53, 53, 53))
			self.palette.setColor(QPalette.ButtonText, Qt.white)
			self.palette.setColor(QPalette.BrightText, Qt.red)
			self.palette.setColor(QPalette.Link, QColor(235, 101, 54))
			self.palette.setColor(QPalette.Highlight, QColor(235, 101, 54))
			self.palette.setColor(QPalette.HighlightedText, Qt.black)
		elif theme == Configuration.THEME_LIGHT:
			self.palette.setColor(QPalette.Window, Qt.white)
			self.palette.setColor(QPalette.WindowText, Qt.black)
			self.palette.setColor(QPalette.Base, QColor(240, 240, 240))
			self.palette.setColor(QPalette.AlternateBase, Qt.white)
			self.palette.setColor(QPalette.ToolTipBase, Qt.white)
			self.palette.setColor(QPalette.ToolTipText, Qt.white)
			self.palette.setColor(QPalette.Text, Qt.black)
			self.palette.setColor(QPalette.Button, Qt.white)
			self.palette.setColor(QPalette.ButtonText, Qt.black)
			self.palette.setColor(QPalette.BrightText, Qt.red)
			self.palette.setColor(QPalette.Link, QColor(66, 155, 248))
			self.palette.setColor(QPalette.Highlight, QColor(66, 155, 248))
			self.palette.setColor(QPalette.HighlightedText, Qt.black)
		self.setPalette(self.palette)


class LoginWidget(QWidget):
	def __init__(self, app):
		super().__init__()
		self.layout = QVBoxLayout()
		self.tabs = QTabWidget()

		self.login_tab = LoginTab(app)
		self.register_tab = RegisterTab(app)

		self.tabs.resize(500, 200)

		self.tabs.addTab(self.login_tab, LoginTab.LABEL)
		self.tabs.addTab(self.register_tab, RegisterTab.LABEL)

		self.layout.addWidget(self.tabs)
		self.setLayout(self.layout)


class LoginTab(QWidget):
	LABEL = 'Вход'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.login_field = QLineEdit(self)
		self.password_field = QLineEdit(self)
		self.remember_me_chb = QCheckBox(self)
		self.init_ui()
		self.setLayout(self.layout)


	def init_ui(self):
		self.layout.addStretch(1)

		login_layout = QHBoxLayout()
		login_label = QLabel('Логин: ')
		login_label.setMinimumWidth(60)
		login_layout.addWidget(login_label)
		self.login_field.setMinimumWidth(200)
		login_layout.addWidget(self.login_field)
		self.layout.addLayout(login_layout)

		password_layout = QHBoxLayout()
		password_label = QLabel('Пароль: ')
		password_label.setMinimumWidth(60)
		password_layout.addWidget(password_label)
		self.password_field.setMinimumWidth(200)
		self.password_field.setEchoMode(QLineEdit.Password)
		password_layout.addWidget(self.password_field)
		self.layout.addLayout(password_layout)

		remember_me_layout = QHBoxLayout()
		remember_me_layout.addWidget(self.remember_me_chb)
		remember_me_label = QLabel('Запомнить пароль?')
		remember_me_layout.addWidget(remember_me_label, 5)
		self.layout.addLayout(remember_me_layout)

		self.layout.addStretch(1)

		login_btn = QPushButton('Вход')
		login_btn.clicked.connect(self.login_event)
		self.layout.addWidget(login_btn)

	def login_event(self):
		if self.app.controller.login(self.login_field.text(), self.password_field.text(), self.remember_me_chb.isChecked()):
			self.app.get_user_widget()
		else:
			QMessageBox.critical(self, 'Ошибка!', 'Неверный логин или пароль!')


class RegisterTab(QWidget):
	LABEL = 'Регистрация'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.name_field = QLineEdit(self)
		self.name_error = QLabel()
		self.login_field = QLineEdit(self)
		self.login_error = QLabel()
		self.password_field = QLineEdit(self)
		self.password_error = QLabel()
		self.conifirm_password_field = QLineEdit(self)
		self.conifirm_password_error = QLabel()
		self.init_ui()
		self.setLayout(self.layout)
		
	def init_ui(self):
		self.layout.addStretch(1)

		name_layout = QHBoxLayout()
		name_label = QLabel('Имя: ')
		name_label.setMinimumWidth(140)
		name_layout.addWidget(name_label)
		self.name_field.setMinimumWidth(200)
		name_layout.addWidget(self.name_field)
		self.layout.addLayout(name_layout)

		login_layout = QHBoxLayout()
		login_label = QLabel('Логин: ')
		login_label.setMinimumWidth(140)
		login_layout.addWidget(login_label)
		self.login_field.setMinimumWidth(200)
		login_layout.addWidget(self.login_field)
		self.layout.addLayout(login_layout)

		password_layout = QHBoxLayout()
		password_label = QLabel('Пароль: ')
		password_label.setMinimumWidth(140)
		password_layout.addWidget(password_label)
		self.password_field.setMinimumWidth(200)
		self.password_field.setEchoMode(QLineEdit.Password)
		password_layout.addWidget(self.password_field)
		self.layout.addLayout(password_layout)

		conifirm_password_layout = QHBoxLayout()
		conifirm_password_label = QLabel('Подтвердите пароль: ')
		conifirm_password_label.setMinimumWidth(140)
		conifirm_password_layout.addWidget(conifirm_password_label)
		self.conifirm_password_field.setMinimumWidth(200)
		self.conifirm_password_field.setEchoMode(QLineEdit.Password)
		conifirm_password_layout.addWidget(self.conifirm_password_field)
		self.layout.addLayout(conifirm_password_layout)

		self.layout.addStretch(1)

		register_btn = QPushButton('Регистрация')
		register_btn.clicked.connect(self.register_event)
		self.layout.addWidget(register_btn)

	def register_event(self):
		register_data = self.app.controller.register(self.name_field.text(), self.login_field.text(), self.password_field.text(), self.conifirm_password_field.text())
		if hasattr(register_data, '__len__') and (not isinstance(register_data, str)):
			QMessageBox.critical(self, 'Ошибка!', register_data[next(iter(register_data))])
		else:
			self.app.get_user_widget()


class MainWidget(QWidget):
	def __init__(self, app):
		super().__init__()
		self.layout = QVBoxLayout()
		self.tabs = QTabWidget()

		self.tabs.resize(500, 200)
		self.profile_tab = ProfileTab(app)
		self.tabs.addTab(self.profile_tab, ProfileTab.LABEL)
		if app.controller.user.is_client() or app.controller.user.is_storekeeper():
			self.products_tab = ProductsTab(app)
			self.tabs.addTab(self.products_tab, ProductsTab.CLIENT_LABEL if app.controller.user.is_client() else ProductsTab.STOREKEEPER_LABEL)
		if app.controller.user.is_client() or app.controller.user.is_manager():
			self.orders_tab = OrdersTab(app)
			self.tabs.addTab(self.orders_tab, OrdersTab.MANAGER_LABEL if app.controller.user.is_manager() else OrdersTab.CLIENT_LABEL)
		if app.controller.user.is_admin():
			self.users_control_tab = UsersControlTab(app)
			self.tabs.addTab(self.users_control_tab, UsersControlTab.LABEL)
		self.settings_tab = SettingsTab(app)
		self.tabs.addTab(self.settings_tab, SettingsTab.LABEL)

		self.layout.addWidget(self.tabs)
		self.setLayout(self.layout)


class ProductsTab(QWidget):
	CLIENT_LABEL = 'Каталог товаров'
	STOREKEEPER_LABEL = 'Управление товарами'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.layout.addWidget(ProductsListWidget(self.app))
		self.setLayout(self.layout)	


class ProductsListWidget(QWidget):
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.scrollable_area = QScrollArea()
		self.scrollable_content = QWidget(self.scrollable_area)
		self.scrollable_layout = QVBoxLayout()
		self.update_list()
		self.init_ui()
		self.setLayout(self.layout)

	def add_labels(self):
		label_l = QHBoxLayout()
		label_l.addWidget(QLabel('ID'), stretch=1)
		label_l.addWidget(QLabel('Наименование'), stretch=8)
		label_l.addWidget(QLabel('Кол-во на складе'), stretch=4)
		label_l.addWidget(QLabel('Действия'), stretch=3)
		self.layout.addLayout(label_l)


	def update_list(self):
		products_list = self.app.controller.get_products()
		self.scrollable_content.destroy(destroyWindow=True, destroySubWindows=True)
		self.scrollable_layout = QVBoxLayout()
		if products_list: 
			for product in products_list: 
				self.scrollable_layout.addWidget(ProductItemWidget(self.app, self, product))
			self.scrollable_layout.addStretch(1)

		self.scrollable_content = QWidget(self.scrollable_area)
		self.scrollable_content.setLayout(self.scrollable_layout)
		self.scrollable_area.setWidgetResizable(True)
		self.scrollable_area.setWidget(self.scrollable_content)

	def init_ui(self):
		self.add_labels()
		self.layout.addWidget(self.scrollable_area)
		upd_btn = QPushButton('Обновить список')
		upd_btn.clicked.connect(self.update_list)
		self.layout.addWidget(upd_btn)
		if self.app.controller.user.is_storekeeper():
			add_btn = QPushButton('Добавить товар')
			add_btn.clicked.connect(self.add_product_event)
			self.layout.addWidget(add_btn)

	def order_event(self, product):
		modal = OrderModal(self, self.app, product)

	def add_product_event(self):
		modal = CreateProductModal(self, self.app)

	def change_product_event(self, product):
		modal = CreateProductModal(self, self.app, product)


class ProductItemWidget(QWidget):
	def __init__(self, app, list_widget, product):
		super().__init__()
		self.app = app
		self.list_widget = list_widget
		self.product = product
		self.layout = QHBoxLayout()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		id_lb = QLabel(str(self.product.id))
		self.layout.addWidget(id_lb, stretch=1)

		name_lb = QLabel(self.product.name)
		self.layout.addWidget(name_lb, stretch=8)

		count_lb = QLabel(str(self.product.count))
		self.layout.addWidget(count_lb, stretch=4 if self.app.controller.user.is_client() or self.app.controller.user.is_storekeeper() else 7)

		if self.app.controller.user.is_client():
			action_btn = QPushButton('Заказать')
			action_btn.clicked.connect(self.order_event)
			self.layout.addWidget(action_btn, stretch=3)

		if self.app.controller.user.is_storekeeper():
			action_btn = QPushButton('Изменить')
			action_btn.clicked.connect(self.change_product_event)
			self.layout.addWidget(action_btn, stretch=3)

	def order_event(self):
		self.list_widget.order_event(self.product)

	def change_product_event(self):
		self.list_widget.change_product_event(self.product)


class OrderModal(QMainWindow):
	def __init__(self, parent, app, product):
		super().__init__(app)
		self.parent = parent
		self.app = app
		self.product = product
		self.setFixedSize(450, 100)
		self.setWindowTitle('Заказ товара')
		self.widget = QWidget()
		self.layout = QVBoxLayout()
		self.count_sl = QSlider(Qt.Horizontal)
		self.count_lb = QLabel('')
		self.init_ui()
		self.widget.setLayout(self.layout)
		self.setCentralWidget(self.widget)
		self.setPalette(self.app.palette)
		self.show()

	def init_ui(self):
		name_l = QHBoxLayout()
		name_l.addWidget(QLabel('Наименование: '), stretch=1)
		name_l.addWidget(QLabel(self.product.name), stretch=10)
		self.layout.addLayout(name_l)

		count_l = QHBoxLayout()
		count_l.addWidget(QLabel('Количество: '), stretch=1)
		self.count_sl.setMinimum(1)
		self.count_sl.setMaximum(self.product.count)
		self.count_sl.setSingleStep(1)
		self.count_sl.setTickPosition(QSlider.TicksBothSides)
		self.count_sl.valueChanged.connect(self.update_count_label)
		count_l.addWidget(self.count_sl, stretch=9)
		self.count_lb.setText(str(self.count_sl.value()))
		count_l.addWidget(self.count_lb, stretch=1)
		self.layout.addLayout(count_l)

		control_l = QHBoxLayout()
		cancel_btn = QPushButton('Отмена')
		cancel_btn.clicked.connect(self.cancel_event)
		control_l.addWidget(cancel_btn)
		ok_btn = QPushButton('Заказать')
		ok_btn.clicked.connect(self.ok_event)
		control_l.addWidget(ok_btn)
		self.layout.addLayout(control_l)

	def update_count_label(self):
		self.count_lb.setText(str(self.count_sl.value()))

	def cancel_event(self):
		self.close()

	def ok_event(self):
		result = self.app.controller.user_make_order(self.product.id, self.count_sl.value())
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			self.parent.update_list()
			QMessageBox.information(self, 'Успех!', 'Товар успешно заказан!')
			self.close()


class CreateProductModal(QMainWindow):
	def __init__(self, parent, app, product = None):
		super().__init__(app)
		self.parent = parent
		self.app = app
		self.product = product
		self.setFixedSize(450, 100)
		self.setWindowTitle('Добавление товара' if self.is_create_mode() else 'Редактирование товара')
		self.widget = QWidget()
		self.layout = QVBoxLayout()
		self.name_le = QLineEdit()
		self.count_le = QLineEdit()
		self.init_ui()
		self.widget.setLayout(self.layout)
		self.setCentralWidget(self.widget)
		self.setPalette(self.app.palette)
		self.show()

	def is_create_mode(self): return not self.product

	def init_ui(self):
		name_l = QHBoxLayout()
		name_l.addWidget(QLabel('Наименование: '), stretch=1)
		if not self.is_create_mode():
			self.name_le.setText(self.product.name)
		name_l.addWidget(self.name_le, stretch=10)
		self.layout.addLayout(name_l)

		count_l = QHBoxLayout()
		count_l.addWidget(QLabel('Количество: '), stretch=1)
		if not self.is_create_mode():
			self.count_le.setText(str(self.product.count))
		count_l.addWidget(self.count_le, stretch=10)
		self.layout.addLayout(count_l)

		control_l = QHBoxLayout()
		cancel_btn = QPushButton('Отмена')
		cancel_btn.clicked.connect(self.cancel_event)
		control_l.addWidget(cancel_btn)
		ok_btn = QPushButton('Создать' if self.is_create_mode() else 'Сохранить')
		ok_btn.clicked.connect(self.ok_event)
		control_l.addWidget(ok_btn)
		self.layout.addLayout(control_l)

	def cancel_event(self):
		self.close()

	def ok_event(self):
		try:
			product_name = self.name_le.text()
			product_count = int(self.count_le.text())
		except Exception as e:
			QMessageBox.critical(self, 'Ошибка!', 'Недопустимые значения полей')
		else:
			if self.is_create_mode():
				result = self.app.controller.storekeeper_create_product(product_name, product_count)
			else:
				result = self.app.controller.storekeeper_update_product(self.product.id, product_name, product_count)

			if isinstance(result, str):
				QMessageBox.critical(self, 'Ошибка!', result)
			else:
				self.parent.update_list()
				QMessageBox.information(self, 'Успех!', 'Товар успешно {}!'.format('создан' if self.is_create_mode() else 'изменён'))
				self.close()


class OrdersTab(QWidget):
	CLIENT_LABEL = 'Мои заказы'
	MANAGER_LABEL = 'Управление заказами'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.layout.addWidget(OrdersListWidget(self.app))
		self.setLayout(self.layout)


class OrdersListWidget(QWidget):
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.scrollable_area = QScrollArea()
		self.scrollable_content = QWidget(self.scrollable_area)
		self.scrollable_layout = QVBoxLayout()
		self.update_list()
		self.init_ui()
		self.setLayout(self.layout)

	def add_labels(self):
		label_l = QHBoxLayout()
		label_l.addWidget(QLabel('ID'), stretch=1)
		label_l.addWidget(QLabel('Наименование'), stretch=3)
		label_l.addWidget(QLabel('Кол-во'), stretch=1)
		label_l.addWidget(QLabel('Статус'), stretch=3 if self.app.controller.user.is_client() else 3)
		if self.app.controller.user.is_manager():
			label_l.addWidget(QLabel('Имя'), stretch=2)
			label_l.addWidget(QLabel('Адресс'), stretch=2)
			label_l.addWidget(QLabel('Телефон'), stretch=2)
		self.layout.addLayout(label_l)


	def update_list(self):
		print('find segmentation')
		orders_list = self.app.controller.get_orders()
		print('get_orders')
		self.scrollable_content.destroy(destroyWindow=True, destroySubWindows=True)
		print('destroy')
		self.scrollable_layout = QVBoxLayout()
		print('new scrollable_layout')
		if orders_list: 
			print('if orders_list')
			for order in orders_list: 
				self.scrollable_layout.addWidget(OrderItemWidget(self.app, self, order))
				print('add 0rder')
			self.scrollable_layout.addStretch(1)
			print('add stretch')

		self.scrollable_content = QWidget(self.scrollable_area)
		print('scrollable_content')
		self.scrollable_content.setLayout(self.scrollable_layout)
		print('scrollable_layout')
		self.scrollable_area.setWidgetResizable(True)
		self.scrollable_area.setWidget(self.scrollable_content)
		print('scrollable_area')

	def init_ui(self):
		self.add_labels()
		self.layout.addWidget(self.scrollable_area)
		upd_btn = QPushButton('Обновить список')
		upd_btn.clicked.connect(self.update_list)
		self.layout.addWidget(upd_btn)


class OrderItemWidget(QWidget):
	def __init__(self, app, list_widget, order):
		super().__init__()
		self.app = app
		self.list_widget = list_widget
		self.order = order
		self.layout = QHBoxLayout()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		id_lb = QLabel(str(self.order.id))
		self.layout.addWidget(id_lb, stretch=1)

		product_name_lb = QLabel(self.order.product.name)
		self.layout.addWidget(product_name_lb, stretch=3)

		count_lb = QLabel(str(self.order.count))
		self.layout.addWidget(count_lb, stretch=1)

		if self.app.controller.user.is_client():
			status_lb = QLabel(self.app.controller.get_order_status(self.order.status))
			self.layout.addWidget(status_lb, stretch=2)

		if self.app.controller.user.is_manager():
			status_cb = QComboBox()
			status_cb.addItems(self.app.controller.get_order_status())
			status_cb.setCurrentIndex(self.order.status)
			status_cb.currentIndexChanged.connect(self.change_order_status_event)
			self.layout.addWidget(status_cb, stretch=2)

			username_lb = QLabel(self.order.user.name)
			self.layout.addWidget(username_lb, stretch=2)

			useraddress_lb = QLabel(self.order.user.address)
			self.layout.addWidget(useraddress_lb, stretch=2)

			userphone_lb = QLabel(self.order.user.phone)
			self.layout.addWidget(userphone_lb, stretch=2)

		if self.app.controller.user.is_client() and not self.order.is_canceled() and not self.order.is_delivered():
			action_btn = QPushButton('Отменить')
			action_btn.clicked.connect(self.cancel_order_event)
			self.layout.addWidget(action_btn, stretch=0)

	def cancel_order_event(self):
		result = self.app.controller.client_cancel_order(self.order.id)
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			QMessageBox.information(self, 'Успех!', 'Заказ успешно Отменён!')
			self.list_widget.update_list()

	def change_order_status_event(self, index):
		result = self.app.controller.manager_change_order_status(self.order.id, index)
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			QMessageBox.information(self, 'Успех!', 'Статус заказа изменён!')
			self.list_widget.update_list()


class ProfileTab(QWidget):
	LABEL = 'Профиль'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.name_le = QLineEdit()
		self.phone_le = QLineEdit()
		self.address_le = QLineEdit()
		self.old_password_le = QLineEdit()
		self.new_password_le = QLineEdit()
		self.conifirm_password_le = QLineEdit()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		change_name_l = QHBoxLayout()
		change_name_lb = QLabel('Имя:')
		change_name_lb.setMinimumWidth(55)
		change_name_lb.setMaximumWidth(65)
		change_name_l.addWidget(change_name_lb)
		self.name_le.setMinimumWidth(390)
		self.name_le.setText(self.app.controller.user.name)
		change_name_l.addWidget(self.name_le)
		change_name_btn = QPushButton('Применить')
		change_name_btn.setMinimumWidth(75)
		change_name_btn.setMaximumWidth(100)
		change_name_btn.clicked.connect(self.change_name_event)
		change_name_l.addWidget(change_name_btn)
		self.layout.addLayout(change_name_l)

		role_l = QHBoxLayout()
		role_lb = QLabel('Роль:')
		role_lb.setMinimumWidth(55)
		role_lb.setMaximumWidth(65)
		role_l.addWidget(role_lb)
		role_l.addWidget(QLabel(self.app.controller.user.role.name))
		self.layout.addLayout(role_l)

		change_phone_l = QHBoxLayout()
		change_phone_lb = QLabel('Телефон:')
		change_phone_lb.setMinimumWidth(55)
		change_phone_lb.setMaximumWidth(65)
		change_phone_l.addWidget(change_phone_lb)
		self.phone_le.setMinimumWidth(390)
		self.phone_le.setText(self.app.controller.user.phone)
		change_phone_l.addWidget(self.phone_le)
		change_phone_btn = QPushButton('Применить')
		change_phone_btn.setMinimumWidth(75)
		change_phone_btn.setMaximumWidth(100)
		change_phone_btn.clicked.connect(self.change_phone_event)
		change_phone_l.addWidget(change_phone_btn)
		self.layout.addLayout(change_phone_l)

		change_address_l = QHBoxLayout()
		change_address_lb = QLabel('Адресс:')
		change_address_lb.setMinimumWidth(55)
		change_address_lb.setMaximumWidth(65)
		change_address_l.addWidget(change_address_lb)
		self.address_le.setMinimumWidth(390)
		self.address_le.setText(self.app.controller.user.address)
		change_address_l.addWidget(self.address_le)
		change_address_btn = QPushButton('Применить')
		change_address_btn.setMinimumWidth(75)
		change_address_btn.setMaximumWidth(100)
		change_address_btn.clicked.connect(self.change_address_event)
		change_address_l.addWidget(change_address_btn)
		self.layout.addLayout(change_address_l)

		change_password_box = QGroupBox('Смена пароля: ')
		change_password_box_layout = QVBoxLayout()

		old_password_l = QHBoxLayout()
		old_password_lb = QLabel('Старый пароль:')
		old_password_lb.setMinimumWidth(100)
		old_password_lb.setMaximumWidth(120)
		old_password_l.addWidget(old_password_lb)
		self.old_password_le.setEchoMode(QLineEdit.Password)
		old_password_l.addWidget(self.old_password_le)
		change_password_box_layout.addLayout(old_password_l)

		new_password_l = QHBoxLayout()
		new_password_lb = QLabel('Новый пароль:')
		new_password_lb.setMinimumWidth(100)
		new_password_lb.setMaximumWidth(120)
		new_password_l.addWidget(new_password_lb)
		self.new_password_le.setEchoMode(QLineEdit.Password)
		new_password_l.addWidget(self.new_password_le)
		change_password_box_layout.addLayout(new_password_l)

		conifirm_password_l = QHBoxLayout()
		conifirm_password_lb = QLabel('Подтверждение:')
		conifirm_password_lb.setMinimumWidth(100)
		conifirm_password_lb.setMaximumWidth(120)
		conifirm_password_l.addWidget(conifirm_password_lb)
		self.conifirm_password_le.setEchoMode(QLineEdit.Password)
		conifirm_password_l.addWidget(self.conifirm_password_le)
		change_password_box_layout.addLayout(conifirm_password_l)

		change_password_btn = QPushButton('Применить')
		change_password_btn.clicked.connect(self.change_password_event)
		change_password_box_layout.addWidget(change_password_btn)

		change_password_box.setLayout(change_password_box_layout)
		self.layout.addWidget(change_password_box)

		self.layout.addStretch(1)

		logout_btn = QPushButton('Выход')
		logout_btn.clicked.connect(self.logout_event)
		self.layout.addWidget(logout_btn)

	def change_name_event(self):
		result = self.app.controller.change_user_name(self.name_le.text())
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			QMessageBox.information(self, 'Успех!', 'Имя изменено!')
		self.name_le.setText(self.app.controller.user.name)

	def change_phone_event(self):
		result = self.app.controller.change_user_phone(self.phone_le.text())
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			QMessageBox.information(self, 'Успех!', 'Телефон изменён!')
		self.phone_le.setText(self.app.controller.user.phone)

	def change_address_event(self):
		result = self.app.controller.change_user_address(self.address_le.text())
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			QMessageBox.information(self, 'Успех!', 'Адресс изменён!')
		self.address_le.setText(self.app.controller.user.address)

	def change_password_event(self):
		result = self.app.controller.change_user_password(self.old_password_le.text(), self.new_password_le.text(), self.conifirm_password_le.text())
		if isinstance(result, str):
			QMessageBox.critical(self, 'Ошибка!', result)
		else:
			QMessageBox.information(self, 'Успех!', 'Пароль изменён!')
		self.old_password_le.setText('')
		self.new_password_le.setText('')
		self.conifirm_password_le.setText('')

	def logout_event(self):
		self.app.controller.logout()
		self.app.get_login_widget()


class UsersControlTab(QWidget):
	LABEL = 'Управление пользователями'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()
		self.layout.addWidget(UsersListWidget(self.app, self.app.controller.admin_get_users_list()))
		self.setLayout(self.layout)	


class UsersListWidget(QWidget):
	def __init__(self, app, users_list):
		super().__init__()
		self.app = app
		self.users_list = users_list
		self.layout = QVBoxLayout()
		self.add_label()
		self.scrollable_area = QScrollArea()
		self.scrollable_content = QWidget(self.scrollable_area)
		self.scrollable_layout = QVBoxLayout()
		self.update_list()
		self.init_ui()
		self.setLayout(self.layout)

	def add_label(self):
		label_l = QHBoxLayout()
		label_l.addWidget(QLabel('ID'), stretch=1)
		label_l.addWidget(QLabel('Логин'), stretch=5)
		label_l.addWidget(QLabel('Имя'), stretch=4)
		label_l.addWidget(QLabel('Роль пользователя'), stretch=4)
		self.layout.addLayout(label_l)

	def update_list(self):
		for i in reversed(range(self.scrollable_layout.count())): self.scrollable_layout.itemAt(i).widget().setParent(None)
		if self.users_list: 
			for user in self.users_list: 
				self.scrollable_layout.addWidget(UsersListItemWidget(self.app, user))
			self.scrollable_layout.addStretch(1)

	def init_ui(self):
		self.scrollable_content.setLayout(self.scrollable_layout)
		self.scrollable_area.setWidgetResizable(True)
		self.scrollable_area.setWidget(self.scrollable_content)
		self.layout.addWidget(self.scrollable_area)


class UsersListItemWidget(QWidget):
	def __init__(self, app, user):
		super().__init__()
		self.app = app
		self.user = user
		self.layout = QHBoxLayout()
		if self.app.controller.user.is_admin():
			self.role_cb = QComboBox()
			self.roles = self.app.controller.get_roles_list()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		id_lb = QLabel(str(self.user.id))
		self.layout.addWidget(id_lb, stretch=1)

		login_lb = QLabel(self.user.login)
		self.layout.addWidget(login_lb, stretch=5)

		name_lb = QLabel(self.user.name)
		self.layout.addWidget(name_lb, stretch=4)

		if self.app.controller.user.is_admin():
			self.fill_roles_list()
			self.role_cb.setCurrentIndex(self.role_cb.findData(self.user.role.id))
			self.role_cb.currentIndexChanged.connect(self.change_user_role_event)
			self.layout.addWidget(self.role_cb, stretch=4)
		else:
			role_lb = QLabel(self.user.role.name)
			self.layout.addWidget(role_lb, stretch=4)

	def fill_roles_list(self):
		if self.roles:
			self.role_cb.clear()
			for role in self.roles:
				self.role_cb.addItem(role.name, userData=role.id)

	def change_user_role_event(self, index):
		old_role = self.user.role.id
		if self.app.controller.admin_change_user_role(self.user, self.roles[index]):
			self.role_cb.setCurrentIndex(index)
		else:
			self.role_cb.setCurrentIndex(self.role_cb.findData(old_role))


class SettingsTab(QWidget):
	LABEL = 'Настройки'
	def __init__(self, app):
		super().__init__()
		self.app = app
		self.layout = QVBoxLayout()

		self.init_ui()

		self.setLayout(self.layout)

	def init_ui(self):
		ui_settings_box = QGroupBox('Внешний вид:')
		ui_settings_lay = QVBoxLayout()

		theme_lay = QHBoxLayout()
		theme_lbl = QLabel('Тема:')
		theme_lay.addWidget(theme_lbl, stretch=1)
		theme_inp = QComboBox()
		theme_inp.addItems(self.app.controller.get_themes())
		theme_inp.setCurrentIndex(self.app.controller.get_theme())
		theme_lay.addWidget(theme_inp, stretch=10)
		theme_inp.currentIndexChanged.connect(self.change_app_theme_event)

		ui_settings_lay.addLayout(theme_lay)
		ui_settings_box.setLayout(ui_settings_lay)

		self.layout.addWidget(ui_settings_box)
		self.layout.addStretch(1)

	def change_app_theme_event(self, index):
		if self.app.controller.set_theme(index):
			self.app.toggle_theme(self.app.controller.get_theme())