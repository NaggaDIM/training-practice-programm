#!/usr/bin/python3
# -*- coding: utf-8 -*-
from peewee import *
import hashlib
import datetime


db = SqliteDatabase('application.db')


class BaseModel(Model):
	class Meta:
		database = db


	def init_database(self):
		db.connect()
		db.create_tables([Configuration, Role, User, Product, ProductLog, Order, OrderLog])
		Configuration.create(key='application_version', value='0.1.5')
		Configuration.create(key='application_theme', value=Configuration.THEME_DARK)
		Role.create(id=Role.ADMINISTRATOR, name=Role.ADMINISTRATOR_DESCRIPTION)
		Role.create(id=Role.DIRECTOR, name=Role.DIRECTOR_DESCRIPTION)
		Role.create(id=Role.MANAGER, name=Role.MANAGER_DESCRIPTION)
		Role.create(id=Role.STOREKEEPER, name=Role.STOREKEEPER_DESCRIPTION)
		Role.create(id=Role.CLIENT, name=Role.CLIENT_DESCRIPTION)
		User.create(name='Администратор', login='Admin', role=Role.ADMINISTRATOR, password=User.hash_password('Root123@'))
		# User.create(name='Директор', login='DIRECTOR', role=Role.DIRECTOR, password=User.hash_password('Root123@'))
		# User.create(name='Менеджер', login='MANAGER', role=Role.MANAGER, password=User.hash_password('Root123@'))
		# User.create(name='Кладовщик', login='STOREKEEPER', role=Role.STOREKEEPER, password=User.hash_password('Root123@'))
		# User.create(name='Клиент', login='CLIENT', role=Role.CLIENT, password=User.hash_password('Root123@'))
		Product.create(name='ATMega8', count=50)
		Product.create(name='ATMega328P', count=10)
		Product.create(name='ATMega8A', count=0)
		Product.create(name='STM32F103C8T6', count=80)
		Product.create(name='STLink v2', count=15)
		Product.create(name='USBAsp Programmer', count=0)


class Configuration(BaseModel):
	id = IntegerField(primary_key=True)
	key = TextField()
	value = TextField()

	THEME_DARK = 0
	THEME_DARK_DESCRIPTION = 'Тёмная'
	THEME_LIGHT = 1
	THEME_LIGHT_DESCRIPTION = 'Светлая'

	THEMES = [THEME_DARK_DESCRIPTION, THEME_LIGHT_DESCRIPTION]


	def get_current_theme(): return int(Configuration.get(key='application_theme').value)
	def set_current_theme(theme):
		dbtheme = Configuration.get(key='application_theme')
		dbtheme.value = str(theme)
		dbtheme.save()


class Role(BaseModel):
	id = IntegerField(primary_key=True)
	name = TextField()

	ADMINISTRATOR = 1
	ADMINISTRATOR_DESCRIPTION = 'Администратор'

	DIRECTOR = 2
	DIRECTOR_DESCRIPTION = 'Директор'

	MANAGER = 3
	MANAGER_DESCRIPTION = 'Менеджер'

	STOREKEEPER = 4
	STOREKEEPER_DESCRIPTION = 'Кладовщик'

	CLIENT = 5
	CLIENT_DESCRIPTION = 'Заказчик'


class User(BaseModel):
	id = IntegerField(primary_key=True)
	name = TextField()
	login = TextField(unique=True)
	role = ForeignKeyField(Role)
	phone = TextField(default='')
	address = TextField(default='')
	password = TextField()

	PASSWORD_REGEX_PATTERN = r"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$"
	PASSWORD_REGEX_INVALID_MESSAGE = 'Слишком слабый пароль!\nПароль должен содержать:\n  {}\n  {}\n  {}\n  {}\n  {}'.format(
			'1. Минимум 8 символов',
			'2. Минимум 1 строчный символ',
			'3. Минимум 1 прописной символ',
			'4. Минимум 1 цифру',
			'5. Минимум 1 спец. символ (!@#$%^&+=)'
		)


	def hash_password(password: str):
		return hashlib.sha224(password.encode('utf-8')).hexdigest()

	def is_admin(self): return self.role.id == Role.ADMINISTRATOR
	def is_director(self): return self.role.id == Role.DIRECTOR
	def is_manager(self): return self.role.id == Role.MANAGER
	def is_storekeeper(self): return self.role.id == Role.STOREKEEPER
	def is_client(self): return self.role.id == Role.CLIENT


class Product(BaseModel):
	id = IntegerField(primary_key=True)
	name = TextField()
	count = IntegerField()


class ProductLog(BaseModel):
	id = IntegerField(primary_key=True)
	product = ForeignKeyField(Product)
	user = ForeignKeyField(User)
	action = IntegerField()
	date = DateTimeField(default=datetime.datetime.now)

	ACTION_CREATED = 0
	ACTION_CREATED_DESCRIPTION = 'Товар создан'

	ACTION_UPDATATED = 1
	ACTION_UPDATATED_DESCRIPTION = 'Информация о товаре была обновлена'

	ACTION_ORDERED = 2
	ACTION_ORDERED_DESCRIPTION = 'Товар заказан'

	ACTIONS = [ACTION_CREATED_DESCRIPTION, ACTION_UPDATATED_DESCRIPTION, ACTION_ORDERED_DESCRIPTION]


class Order(BaseModel):
	id = IntegerField(primary_key=True)
	user = ForeignKeyField(User)
	product = ForeignKeyField(Product)
	count = IntegerField()
	status = IntegerField()

	STATUS_CREATED = 0
	STATUS_CREATED_DESCRIPTION = 'Создан'

	STATUS_SENDED = 1
	STATUS_SENDED_DESCRIPTION = 'Отправлен'

	STATUS_DELIVERED = 2
	STATUS_DELIVERED_DESCRIPTION = 'Доставлен'

	STATUS_CANCELED = 3
	STATUS_CANCELED_DESCRIPTION = 'Отменён'

	STATUSES = [STATUS_CREATED_DESCRIPTION, STATUS_SENDED_DESCRIPTION, STATUS_DELIVERED_DESCRIPTION, STATUS_CANCELED_DESCRIPTION]

	def is_created(self): return self.status == self.STATUS_CREATED
	def is_sended(self): return self.status == self.STATUS_SENDED
	def is_delivered(self): return self.status == self.STATUS_DELIVERED
	def is_canceled(self): return self.status == self.STATUS_CANCELED


class OrderLog(BaseModel):
	id = IntegerField(primary_key=True)
	order = ForeignKeyField(Order)
	user = ForeignKeyField(User)
	action = IntegerField()
	date = DateTimeField(default=datetime.datetime.now)

	ACTION_CREATED = 0
	ACTION_CREATED_DESCRIPTION = 'Заказ создан'

	ACTION_SENT = 1
	ACTION_SENT_DESCRIPTION = 'Статус изменён на "Отправлен"'

	ACTION_DELIVERED = 2
	ACTION_DELIVERED_DESCRIPTION = 'Статус изменён на "Доставлен"'

	ACTION_CANCELED = 3
	ACTION_CANCELED_DESCRIPTION = 'Статус изменён на "Отменён"'

	ACTIONS = [ACTION_CREATED_DESCRIPTION, ACTION_SENT_DESCRIPTION, ACTION_DELIVERED_DESCRIPTION, ACTION_CANCELED_DESCRIPTION]